# Ryuutama for Foundry VTT

This is a very simple implementation of character and item sheets for use when
playing [Ryuutama](https://kotohi.com/ryuutama/) in 
[Foundry VTT](https://foundryvtt.com/). There is minimal automation, but the UI 
is (hopefully) far more pleasant to use than the Simple World-Building system.
