class RyuutamaItem extends Item {
	static getDefaultArtwork(itemData) {
		let path = "icons/svg/item-bag.svg";
		if (itemData.type == "weapon") {
			path = "icons/svg/sword.svg";
		} else if (itemData.type == "equipment") {
			path = "icons/svg/shield.svg";
		}

		return {
			img: path,
			texture: {
				src: path,
			}
		}
	}

	prepareData() {
		super.prepareData();
		console.log("FAT NUT");
	}
}

class RyuutamaItemSheet extends ItemSheet {
	static get defaultOptions() {
		return foundry.utils.mergeObject(super.defaultOptions, {
		classes: ["ryuutama", "sheet", "item", "basic"],
		width: 600,
		height: 600,
		tabs: []
		});
	}

	get template() {
		const path = "systems/ryuutama/templates/item-sheets";
		return `${path}/${this.item.type}-sheet.hbs`;
	}

	// Pretty much the same as the ActorSheet equivalent
	async getData(options) {
		const context = super.getData(options);
		context.system = context.item.system;
		context.descriptionHTML = await TextEditor.enrichHTML(context.system.description, {
			secrets: this.document.isOwner,
		});
		return context;
	}
}

class RyuutamaActor extends Actor {
	// Default artwork is different for different types of actor
	static getDefaultArtwork(actorData) {
		let path = "systems/ryuutama/icons/farmer.png";
		if (actorData.type == "monster") {
			path = "systems/ryuutama/icons/koneko-goblin.png";
		}

		return {
			img: path,
			texture: {
				src: path,
			},
		};
	}

	// Handles data derived from other fields. For example, character levels 
	// are determined by how much XP the character has.
	prepareData() {
		super.prepareData();

		// Bespoke handling for different actor types
		if (this.type == "character") {
			this._prepareCharacterData();
		} else if (this.type == "monster") {
			this._prepareMonsterData();
		}
	}

	_prepareCharacterData() {
		// Determine level based on XP
		const system = this.system;
		let level = 0;
		if (system.attributes.xp < 100) {
			level = 1;
		} else if (system.attributes.xp < 600) {
			level = 2;
		} else if (system.attributes.xp < 1200) {
			level = 3;
		} else if (system.attributes.xp < 2000) {
			level = 4;
		} else if (system.attributes.xp < 3000) {
			level = 5;
		} else if (system.attributes.xp < 4200) {
			level = 6;
		} else if (system.attributes.xp < 5800) {
			level = 7;
		} else if (system.attributes.xp < 7500) {
			level = 8;
		} else if (system.attributes.xp < 10000) {
			level = 9;
		} else {
			level = 10;
		}

		system.attributes.level = level;
	}

	_prepareMonsterData() {
		// Probably won't need to do anything here anytime soon...
	}
}

class RyuutamaActorSheet extends ActorSheet {
	static get defaultOptions() {
		return foundry.utils.mergeObject(super.defaultOptions, {
		classes: ["ryuutama", "sheet", "actor", "character"],
		width: 600,
		height: 600,
		tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "details" }]
		});
	}

	// Different sheets for different actor types
	get template() {
		const path = "systems/ryuutama/templates/actor-sheets";
		return `${path}/${this.actor.type}-sheet.hbs`;
	}

	async getData(options) {
		const context = super.getData(options);

		// Standard convenience assignment, means you just refer to {{system}} 
		// in HandleBars code instead of {{actor.system}}.
		context.system = context.actor.system;

		// enrichHTML presumably keeps us safe from cross-site scripting or 
		// other injection vulnerabilities
		context.biographyHTML = await TextEditor.enrichHTML(context.system.biography, {
			secrets: this.document.isOwner,
		});

		// We want to categorise the different types of items on the character sheet
		context.equipped = {
			weapons: [],
			equipment: [],
		};
		context.weapons = [];
		context.equipment = [];
		context.basicItems = [];
		context.remainingCapacity = context.system.attributes.capacity;
		context.hasEquipment = false;

		// Sort items into appropriate categories. Note that equipped stuff does not
		// take away from remaining carrying capacity.
		context.items.forEach((item) => {
			if (item.type == "weapon") {
				if (item.system.equipped) {
					context.equipped.weapons.push(item);
					context.hasEquipment = true;
				} else {
					context.weapons.push(item);
					context.remainingCapacity -= item.system.size;
				}
			} else if (item.type == "equipment") {
				if (item.system.equipped) {
					context.equipped.equipment.push(item);
					context.hasEquipment = true;
				} else {
					context.equipment.push(item);
					context.remainingCapacity -= item.system.size;
				}
			} else {
				// Some basic items are stackable. We put this information at the top level to
				// make some templating a bit easier, and we use it to influence remaining carry
				// capacity.
				let quantity = 1;
				item.stackable = item.type === "basic" && item.system.stackable;
				item.quantityString = "";
				if (item.stackable && item.system.quantity > 1) {
					quantity = item.system.quantity;
					item.quantityString = `x${item.system.quantity}`;
				}

				context.basicItems.push(item);
				context.remainingCapacity -= item.system.size * quantity;
			}
		});

		// All items are sorted by name, but basic items are also grouped up into separate
		// categories that aren't explicitly shown on the character sheets. This just ensures
		// similar items are closer together on the inventory page.
		context.weapons.sort((a, b) => { return a.name < b.name; });
		context.equipment.sort((a, b) => { return a.name < b.name; });
		context.basicItems.sort((a, b) => { 
			if (a.system.group == b.system.group) {
				return a.name > b.name; 
			} else {
				return a.system.group > b.system.group;
			}
		});

		return context;
	}

	activateListeners(html) {
		super.activateListeners(html);

		// Allow opening the item sheet
		html.on('click', '.item-details-button', (ev) => {
			const div = $(ev.currentTarget).parents('.item-name');
			const item = this.actor.items.get(div.data('itemId'));
			item.sheet.render(true);
		});

		// Anything after this point is only for when the sheet is editable
		if (!this.isEditable) return;

		// Handles equipping an item
		html.on('click', '.item-equip', (ev) => {
			const div = $(ev.currentTarget).parents('.item-buttons');
			const item = this.actor.items.get(div.data('itemId'));
			//item.system.equipped = true;
			item.update({ system: { equipped: true } });
			this.render(false);
		})

		// Handles unequipping an item
		html.on('click', '.item-unequip', (ev) => {
			const div = $(ev.currentTarget).parents('.item-buttons');
			const item = this.actor.items.get(div.data('itemId'));
			item.update({ system: { equipped: false} });
			this.render(false);
		})
		
		// Handles using an item. This is the same as dropping, just without any confirmation
		html.on('click', '.item-use', async (ev) => {
			// Get the item object based on its ID recorded in the parent element
			const div = $(ev.currentTarget).parents('.item-buttons');
			const item = this.actor.items.get(div.data('itemId'));
			if (item.type === "basic") {
				item.system.quantity -= 1;

				if (item.system.quantity > 0) {
					item.update({ system: { quantity: item.system.quantity }});
				} else {
					item.delete();
				}

				div.slideUp(200, () => this.render(false));
			} 
		});

		// Handles dropping/deleting an item
		html.on('click', '.item-drop', async (ev) => {
			// Get the item object based on its ID recorded in the parent element
			const div = $(ev.currentTarget).parents('.item-buttons');
			const item = this.actor.items.get(div.data('itemId'));
			
			// Get confirmation, since this is not always what a player wants!
			const confirmation = await Dialog.confirm({
				title: "Drop Confirmation",
				content: `Are you sure you want to drop this ${item.name} stack?`,
				defaultYes: false
			});

			if (confirmation) {
				item.delete();
				div.slideUp(200, () => this.render(false));
			}
		});

		// Handles stacking a single extra item
		html.on('click', '.item-stack-1', (ev) => {
			const div = $(ev.currentTarget).parents('.item-buttons');
			const item = this.actor.items.get(div.data('itemId'));
			item.update({ system: { quantity: item.system.quantity + 1 }});
			this.render(false);
		});	

		// Handles stacking 5 extra items at once
		html.on('click', '.item-stack-5', (ev) => {
			const div = $(ev.currentTarget).parents('.item-buttons');
			const item = this.actor.items.get(div.data('itemId'));
			item.update({ system: { quantity: item.system.quantity + 5 }});
			this.render(false);
		});	
	}
}

// Partial sheets included in main ones (such as the actor-header for character 
// and monster sheets) have to be preloaded.
async function preloadPartials() {
	const templatePaths = [
		"systems/ryuutama/templates/actor-sheets/parts/actor-header.hbs",
		"systems/ryuutama/templates/actor-sheets/parts/actor-item-header.hbs",
		"systems/ryuutama/templates/actor-sheets/parts/actor-main-stats.hbs",
		"systems/ryuutama/templates/item-sheets/parts/item-header.hbs",
	];
	return loadTemplates(templatePaths);
}

// Handle system initialisation, replacing defaults with Ryuutama-specifics
Hooks.once("init", async function() {
	CONFIG.Actor.documentClass = RyuutamaActor;
	CONFIG.Item.documentClass = RyuutamaItem;

	Actor.DEFAULT_ICON = "systems/ryuutama/icons/farmer.png";
	Actors.unregisterSheet("core", ActorSheet);
	Actors.registerSheet("ryuutama", RyuutamaActorSheet, {
		makeDefault: true
	});

	Items.unregisterSheet("core", ItemSheet);
	Items.registerSheet("ryuutama", RyuutamaItemSheet, {
		makeDefault: true
	});

	await preloadPartials();
});
